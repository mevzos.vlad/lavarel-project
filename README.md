<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Intro
This project was created for **Innoscripta Devops Task** by **Vladimir Mevzos**

# Docker
Implement docker to this repository
## Local Development
There are two flavours of docker for local development: docker-compose and docker-nginx.

### Docker compose
**docker-compose** provide better isolation, but it could be more challenging to deploy and manage,
since there are more moving parts.
  `docker-compose.yml` consists of 4 dockers composed together:
  * **app**  Lavarel Service
  * **nginx** Nginx Front-end webserver
  * **mysql** MySQL store layer
  * **redis** Cache layer  

**app** Lavarel Service available based on `php:7.4-fpm-alpine3.12` and  available in two variants:
* `docker-compose/app_alpine_slim.dockerfile` (*default*) docker image with minimal set of extensions
* `docker-compose/app_alpine.dockerfile` docker image about with extra extensions which are commonly used in real-life use cases
It is simple to tweak this in `docker-compose.yml`

In order to deploy this stack locally it is enough to do just:
```
docker-compose up -d
````
### Docker Nginx
**docker-nginx** is easier to manage and further in this Devops task i do use this flavour to deploy it to ECS or EKS. docker-nginx might also require persistence layer, like MySQL or MariaDB and cache, like Redis. Management those layers is outside of this test task and should be treated separately. Since EKS and ECS are the best for stateless services, statefull services like DBs or Message queues can/should be outide of ECS/EKS services

  `Dockerfile` This docker image also based on `php:7.4-fpm-alpine3.12`. It does include lavarel service, nginx and supervisor in same docker image. It is exposing port 80.          

In order to test this image locally:
  ```
  # build docker image
  docker build -t lavarel-app  .
  # run docker image
  docker run -d -p 80:80 <image_id>
  ````

# Gitlab CI/CD
Implement GitLab CI/CD to this repository

`.gitlab-ci.yml`  - you can see all CI/CD pipeline.

## Enough Context
**Before running this CI/CD pipeline you should provide enough context and set up variables in `.gitlab-ci.yml` file**
There are bunch of variables should be defined before running pipeline. Some of the most important ones are $AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY $AWS_DEFAULT_REGION and REPOSITORY_URL.

 It is good practice to keep most of those variables outside of  `.gitlab-ci.yml` and define them on project level as variables. Motivation for putting them inside *yml*  is allow changes to it without having access to the project variables.


## Stages
There are four stages defined:
* **build**  build docker image and push it to repository
* **test**   do sample tests using phpunit
* **predeploy** used to tag image with master tag , if tests completed successfully
* **deploy**   used to deploy in parralel to different destinations: EC2, ECS, EKS

**build** and **test** stages are running when MR is created
**predeploy** and **deploy** stages are running when there are any changes to master


## Predeploy
There is one more stage in pipeline called **predeploy**.  The idea behind is to add extra tag `master` to the image after it successfully passed all the tests. This tag indicate docker image deployed in ECS/EKS. Drawback of this approach is that in current implementation there will be another docker pull operation , means extra time and traffic. This can be avoided by having own gitlab-runners, where good chances are that image is already present and need to just to add tag, means cheap operation. Also it is possible to use registries API to tag images in-place.

## Docker registry         
In this setup used public ECR. Other options on the table are private ECR, Gitlab docker registry or dedicated docker registry solution like Artifactory https://jfrog.com/artifactory/ . Unfortunately, `gitlab-ci.yml` will be slightly different, mostly related to the command to login to those registries  

```
# public ECR
aws ecr-public get-login-password --region us-east-1 | docker login -u AWS --password-stdin $REPOSITORY_URL
# private ECR
aws ecr get-login --no-include-email --region "${AWS_DEFAULT_REGION}")
# Gitlab registry
docker login -u $USER_NAME -p $USER_PASSWORD $REPOSITORY_URL
```

## Extra Steps
It is possible to automatically create MR if there was any push to branch. There is pretty outdated blog about this:  https://about.gitlab.com/blog/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/

# Tests Gitlab CI
Currently only sample tests are running during test stage. It is done using **phpunit**

## System Tests
In real production environment different testing aspects should be covered including system tests (Lavarel vs MySQL and Lavarel vs Redis). This can be done in Gitlab CI by adding related services to the test stage  

## Smoke Test
build stage is *before* test. Motivation here is to test *exactly same* docker image which will be deployed. This will help to prevent dependencies issues coming from build process itself. Drawback of this approach is that, even if code will not pass any tests it still would fail only *after* build stage, it can slow down development process. Better approach would be to add **smoketest** stage before the build. But this is out of scope this test task  

# Deploy

## Deploy to EC2
`deploy_ec2` One of the options is to deploy to AWS EC2 on-demand instance. Instance should be created using defined key-pair, private key $SSH_PRIVATE_KEY will be used by gitlab-runners to ssh to instance and perform deploy operations. EC2 instance should have docker installed.

It is pretty simple to tweak process to use list of EC2 instances to deploy app

### Elastic IP
gitlab-runners running outside of AWS, therefore most simple way is to use ElasticIP to allow gitlab-runners to ssh. In real life scenario this can be avoided if gitlab-runners will run in the same AWS account    

## Deploy to ECS
`deploy_ecs`
There are some assumptions:
* ECS cluster with $CLUSTER_NAME does exist
* Task Definiton $TASK_DEFINITION_NAME does exist
* Service $SERVICE_NAME was created

Those resources might be created manually or using terraform for example. Benefit of using ECS over EKS is simplicity.
Deploy is pretty straightforward:
* Updating existing task definition with new docker image
* Updating service to use updated task definition  

## Deploy to EKS

Here are the steps to create new EKS cluster from scratch and deploy app to it using Gitlab CI/CD:
* create new AWS user with programmatic access
* create AWS policies ( check next AWS IAM part)
* attach policies to the new AWS user
* configure your local computer to use new AWS user with `awscli`
* instal eksctl
* create new EKS cluster using eksctl
* tweak variables in project to fit to your environment

### create EKS via eksctl
`deploy_eks` For test purporses we will create EKS cluster using eksctl https://eksctl.io/introduction/ using new AWS user and will deploy app to this EKS cluster.

To install `eksctl` on macOS:
`brew install weaveworks/tap/eksctl`

`eks/eks_cluster.yaml` here you can find cluster template:

```
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: eks-test
  region: eu-central-1

nodeGroups:
  - name: ng-1
    instanceType: m5.large
    desiredCapacity: 2
```

To create EKS cluster run this command:

`eksctl create cluster -f eks/eks_cluster.yaml`

### EKS Deployment via CI/CD
After cluster is ready deployment is pretty simple :
* installing kubectl on gitlab-runner
* getting context about cluster
* updating Deployment with new docker image  

### Next Steps
It would be nice to have Integration with Ingress Controller as well.  

# AWS IAM
User created in AWS should have following policies attached:
## EKS related policies
* `aws/AmazonEC2FullAccess.json` (AWS managed policy)
* `aws/AWSCloudFormationFullAccess.json` (AWS managed policy)
* `aws/eksAllAccess.json`
* `aws/IamLimitedAccess.json`
## ECS  related policies
* `aws/ecsDeploy.json`
## ECR related policies
* `aws/ecrAccess.json`
* `aws/GetAuthorizationToken.json`

Please take a look at related files to get more context.

Do not forget to replace <account_id> with relevant AWS account_id

Thank you for your attention ) 
