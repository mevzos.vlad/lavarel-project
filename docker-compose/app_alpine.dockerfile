### Docker image is about 430MB ###

FROM php:7.4-fpm-alpine3.12

LABEL Maintainer Vladimir Mevzos <mevzos.vlad@gmail.com>

# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/

# Setup Working Dir
WORKDIR /var/www

# Add Repositories
RUN rm -f /etc/apk/repositories &&\
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/community" >> /etc/apk/repositories

# Add Build Dependencies
RUN apk add --no-cache --virtual .build-deps  \
    zlib-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libxml2-dev \
    bzip2-dev \
    zip \
    libzip-dev

# Add Production Dependencies
RUN apk add --update --no-cache \
    jpegoptim \
    pngquant \
    optipng \
    icu-dev \
    freetype-dev
    #nginx \
    #mysql-client

# Configure & Install Extensions
RUN docker-php-ext-configure \
    opcache --enable-opcache &&\
    docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include/ && \
    docker-php-ext-install \
    opcache \
    mysqli \
    pdo \
    pdo_mysql \
    sockets \
    json \
    intl \
    gd \
    xml \
    bz2 \
    pcntl \
    bcmath \
    zip
# Redis support installation
RUN apk add --no-cache \
    $PHPIZE_DEPS &&\
    pecl install redis &&\
    docker-php-ext-enable redis

# Download and install composer
ARG COMPOSER_COMMIT_HASH=da7be05fa1c9f68b9609726451d1aaac7dd832cb
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/${COMPOSER_COMMIT_HASH}/web/installer -O - -q | php -- --quiet
RUN chmod +x /var/www/composer.phar \
    && ln -s /var/www/composer.phar /usr/bin/composer


# Add user for laravel application
RUN addgroup -g 1000 innoscripta
RUN adduser -D -s /bin/sh -u 1000 -G innoscripta innoscripta

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
RUN chown innoscripta:innoscripta /var/www

# Change current user to www
USER innoscripta

# Expose port 9000
EXPOSE 9000

# Run app
CMD ["php-fpm"]
