FROM php:7.4-fpm-alpine3.12

LABEL Maintainer Vladimir Mevzos <mevzos.vlad@gmail.com>

# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/

# Setup Working Dir
WORKDIR /var/www

# Add Repositories
RUN rm -f /etc/apk/repositories &&\
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/community" >> /etc/apk/repositories

# Add Build Dependencies
RUN apk add --no-cache --virtual .build-deps  \
    zlib-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libxml2-dev \
    bzip2-dev \
    zip \
    libzip-dev\
    freetype-dev\
    nginx \
    curl \
    supervisor

# Configure & Install Extensions
RUN docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include/ && \
    docker-php-ext-install \
    mysqli \
    pdo \
    pdo_mysql \
    zip \
    gd

# Download and install composer
ARG COMPOSER_COMMIT_HASH=da7be05fa1c9f68b9609726451d1aaac7dd832cb
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/${COMPOSER_COMMIT_HASH}/web/installer -O - -q | php -- --quiet
RUN chmod +x /var/www/composer.phar \
    && ln -s /var/www/composer.phar /usr/bin/composer

RUN mkdir -p /run/nginx

# Copy existing application directory contents
COPY . /var/www
COPY docker-nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY docker-nginx/supervisord.conf /etc/supervisord.conf

# Copy existing application directory permissions
RUN chown -R root:www-data /var/www && \
    chmod -R 777 /var/www && \
    chmod -R 777 /var/www/public

# Prepare env and artisan
RUN cp .env.example .env && \
    composer install && \
    php artisan key:generate

# Expose port 80
EXPOSE 80

# Run app with supervisor
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
